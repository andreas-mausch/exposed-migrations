package de.neonew.exposed.migrations.tests.integration

import de.neonew.exposed.migrations.Migrations
import de.neonew.exposed.migrations.runMigrations
import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction

object ExistingTable : IntIdTable()

class NonEmptyDatabaseTest : IntegrationTest({
  "succeed if migrations are run against a empty database" {
    shouldNotThrow<IllegalStateException> {
      runMigrations(database, listOf(), clock)
    }
  }

  "succeed if migrations are run against a non-empty database with a migrations table" {
    transaction(database) { create(Migrations) }
    transaction(database) { create(ExistingTable) }

    shouldNotThrow<IllegalStateException> {
      runMigrations(database, listOf(), clock)
    }
  }

  "fail if migrations are run against a non-empty database without a migrations table" {
    transaction(database) { create(ExistingTable) }

    val exception = shouldThrow<IllegalStateException> {
      runMigrations(database, listOf(), clock)
    }
    exception.message shouldBe "Tried to run migrations against a non-empty database without a Migrations table. This is not supported."
  }
})
