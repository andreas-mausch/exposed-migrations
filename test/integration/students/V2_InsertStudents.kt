package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.insert

internal class V2_InsertStudents : Migration() {
  object Students : IntIdTable() {
    val name = varchar("name", length = 100)
    val dateOfBirth_string = varchar("date_of_birth", length = 100)
  }

  override fun run() {
    Students.insert {
      it[name] = "Alex"
      it[dateOfBirth_string] = "2000-01-30"
    }
    Students.insert {
      it[name] = "Jimi"
      it[dateOfBirth_string] = "1999-01-30"
    }
  }
}
