package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils

internal class V1_CreateStudents : Migration() {
  object Students : IntIdTable() {
    val name = varchar("name", length = 100)
    val dateOfBirth_string = varchar("date_of_birth", length = 100)
  }

  override fun run() {
    SchemaUtils.create(Students)
  }
}
