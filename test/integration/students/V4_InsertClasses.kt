package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.insert

internal class V4_InsertClasses : Migration() {
  object Classes : IntIdTable() {
    val name = varchar("name", length = 100)
  }

  override fun run() {
    Classes.insert {
      it[name] = "Maths"
    }
    Classes.insert {
      it[name] = "English"
    }
    Classes.insert {
      it[name] = "Biology"
    }
  }
}
