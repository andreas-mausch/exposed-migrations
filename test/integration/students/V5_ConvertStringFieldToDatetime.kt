package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.date
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.update
import java.time.LocalDate

internal class V5_ConvertStringFieldToDatetime : Migration() {
  object Students : IntIdTable() {
    val name = varchar("name", length = 100)
    val dateOfBirth_string = varchar("date_of_birth", length = 100)
    val dateOfBirth_date = date("date_of_birth_date").nullable()
  }

  object StudentsNonNullable : IntIdTable("Students") {
    val dateOfBirth_date = date("date_of_birth_date")
  }

  override fun run() {
    Students.dateOfBirth_date.createStatement().forEach { TransactionManager.current().exec(it) }
    Students.selectAll().map { row ->
      val date = LocalDate.parse(row[Students.dateOfBirth_string])
      Students.update({ Students.id eq row[Students.id] }) {
        it[dateOfBirth_date] = date
      }
    }

    // https://github.com/JetBrains/Exposed/issues/911
    // StudentsNonNullable.dateOfBirth_date.modifyStatement().forEach { TransactionManager.current().exec(it) }
    TransactionManager.current().exec("ALTER TABLE STUDENTS ALTER COLUMN DATE_OF_BIRTH_DATE DATE NOT NULL")
  }
}
