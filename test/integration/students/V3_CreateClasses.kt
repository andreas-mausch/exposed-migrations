package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils

internal class V3_CreateClasses : Migration() {
  object Classes : IntIdTable() {
    val name = varchar("name", length = 100)
  }

  override fun run() {
      SchemaUtils.create(Classes)
  }
}
