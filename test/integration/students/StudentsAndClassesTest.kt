package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.runMigrations
import de.neonew.exposed.migrations.tests.integration.IntegrationTest
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate.parse

class StudentsAndClassesTest : IntegrationTest({
  "v1" {
    runMigrations(database, listOf(V1_CreateStudents()), clock)

    transaction(database) {
      V1_CreateStudents.Students.selectAll().count() shouldBe 0
    }
  }

  "v2" {
    runMigrations(database, listOf(
      V1_CreateStudents(),
      V2_InsertStudents()
    ), clock)

    transaction(database) {
      V2_InsertStudents.Students.selectAll().count() shouldBe 2
    }
  }

  "v3" {
    runMigrations(database, listOf(
      V1_CreateStudents(),
      V2_InsertStudents(),
      V3_CreateClasses()
    ), clock)

    transaction(database) {
      V3_CreateClasses.Classes.selectAll().count() shouldBe 0
    }
  }

  "v4" {
    runMigrations(database, listOf(
      V1_CreateStudents(),
      V2_InsertStudents(),
      V3_CreateClasses(),
      V4_InsertClasses()
    ), clock)

    transaction(database) {
      V4_InsertClasses.Classes.selectAll().count() shouldBe 3
    }
  }

  "v5" {
    runMigrations(database, listOf(
      V1_CreateStudents(),
      V2_InsertStudents(),
      V3_CreateClasses(),
      V4_InsertClasses(),
      V5_ConvertStringFieldToDatetime()
    ), clock)

    transaction(database) {
      val students = V5_ConvertStringFieldToDatetime.StudentsNonNullable
      students.selectAll().map { it[students.id].value to it[students.dateOfBirth_date] } shouldBe
        listOf(1 to parse("2000-01-30"), 2 to parse("1999-01-30"))
    }

    // Ensure no null values are allowed for the new column
    transaction(database) {
      val exception = shouldThrow<ExposedSQLException> {
        V5_ConvertStringFieldToDatetime.Students.insert {
          it[name] = "John"
          it[dateOfBirth_string] = "1998-01-30"
        }
      }
      exception.message shouldContain "JdbcSQLIntegrityConstraintViolationException"
      exception.message shouldContain "NULL not allowed for column \"DATE_OF_BIRTH_DATE\""
    }
  }

  "v6" {
    runMigrations(database, listOf(
      V1_CreateStudents(),
      V2_InsertStudents(),
      V3_CreateClasses(),
      V4_InsertClasses(),
      V5_ConvertStringFieldToDatetime(),
      V6_ManyToManyRelation()
    ), clock)

    transaction(database) {
      val studentsToClasses = V6_ManyToManyRelation.StudentsToClasses
      val relations = studentsToClasses.selectAll().map {
        it[studentsToClasses.student].value to it[studentsToClasses.`class`].value
      }
      relations shouldBe listOf(1 to 2)
    }
  }
})
