package de.neonew.exposed.migrations.tests.integration.students

import de.neonew.exposed.migrations.Migration
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.Table

internal class V6_ManyToManyRelation : Migration() {
  object Students : IntIdTable()
  object Classes : IntIdTable()

  object StudentsToClasses : Table() {
    val student = reference("student", Students)
    val `class` = reference("class", Classes)
    override val primaryKey = PrimaryKey(student, `class`)
  }

  class Student(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Student>(Students)

    var classes by Class via StudentsToClasses
  }

  class Class(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Class>(Classes)
  }

  override fun run() {
    create(StudentsToClasses)

    val student = Student.findById(1)!!
    val clazz = Class.findById(2)!!

    student.classes = SizedCollection(listOf(clazz))
  }
}
