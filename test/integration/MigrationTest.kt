package de.neonew.exposed.migrations.tests.integration

import de.neonew.exposed.migrations.Migration
import de.neonew.exposed.migrations.MigrationEntity
import de.neonew.exposed.migrations.runMigrations
import io.kotest.matchers.collections.shouldExist
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

object TestTable : IntIdTable() {
  val someField = integer("version")

  init {
    index(true, someField)
  }
}

private class V1_TestTableMigration : Migration() {
  override fun run() {
    create(TestTable)
  }
}

class MigrationTest : IntegrationTest({
  "run test migration" {
    runMigrations(database, listOf(V1_TestTableMigration()), clock)

    transaction(database) {
      val foundMigrations = MigrationEntity.all().toList()
      foundMigrations shouldHaveSize 1
      foundMigrations shouldExist {
        it.name == "TestTableMigration"
          && it.version.value == 1
          && it.executedAt == Instant.parse("2020-01-30T09:59:59.123Z")
      }
      TestTable.exists() shouldBe true
    }
  }

  "don't run same migration twice" {
    // given
    runMigrations(database, listOf(V1_TestTableMigration()), clock)

    // when
    runMigrations(database, listOf(V1_TestTableMigration()), clock)

    // then
    transaction(database) {
      val foundMigrations = MigrationEntity.all().toList()
      foundMigrations shouldHaveSize 1
      TestTable.exists() shouldBe true
    }
  }
})
