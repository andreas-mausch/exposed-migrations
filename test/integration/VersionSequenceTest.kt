package de.neonew.exposed.migrations.tests.integration

import de.neonew.exposed.migrations.Migration
import de.neonew.exposed.migrations.runMigrations
import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe

private class V1_TestMigration : Migration() {
  override fun run() {}
}

private class V1_DuplicationTestMigration : Migration() {
  override fun run() {}
}

private class V2_TestMigration2 : Migration() {
  override fun run() {}
}

private class V3_TestMigration3 : Migration() {
  override fun run() {}
}

class VersionSequenceTest : IntegrationTest({
  "if we got a sequence of versions number, no exception is thrown" {
    shouldNotThrow<IllegalStateException> {
      runMigrations(database, listOf(V1_TestMigration(), V2_TestMigration2(), V3_TestMigration3()), clock)
    }
  }

  "if a version number is duplicated, no migrations should run" {
    val exception = shouldThrow<IllegalStateException> {
      runMigrations(database, listOf(V1_TestMigration(), V1_DuplicationTestMigration()), clock)
    }
    exception.message shouldBe "List of migrations version is not consecutive: [1, 1]"
  }

  "if there is a gap in the versions numbering, no migrations should run" {
    val exception = shouldThrow<IllegalStateException> {
      runMigrations(database, listOf(V1_TestMigration(), V3_TestMigration3()), clock)
    }
    exception.message shouldBe "List of migrations version is not consecutive: [1, 3]"
  }
})
