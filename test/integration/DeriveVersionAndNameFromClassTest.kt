package de.neonew.exposed.migrations.tests.integration

import de.neonew.exposed.migrations.Migration
import de.neonew.exposed.migrations.MigrationEntity
import de.neonew.exposed.migrations.runMigrations
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldExist
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

private class V1_MigrationWithValidName : Migration() {
  override fun run() {}
}

private class Vabc_MigrationWithInvalidName : Migration() {
  override fun run() {}
}

class DeriveVersionAndNameFromClassTest : IntegrationTest({
  "valid migration" {
    runMigrations(database, listOf(V1_MigrationWithValidName()), clock)

    transaction(database) {
      val foundMigrations = MigrationEntity.all().toList()
      foundMigrations shouldHaveSize 1
      foundMigrations shouldExist {
        it.name == "MigrationWithValidName"
          && it.version.value == 1
          && it.executedAt == Instant.parse("2020-01-30T09:59:59.123Z")
      }
    }
  }

  "invalid migration" {
    val exception = shouldThrow<IllegalArgumentException> {
      runMigrations(database, listOf(Vabc_MigrationWithInvalidName()), clock)
    }
    exception.message shouldBe "Migration class name doesn't match convention"
  }
})
