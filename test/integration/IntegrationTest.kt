package de.neonew.exposed.migrations.tests.integration

import com.statemachinesystems.mockclock.MockClock
import com.statemachinesystems.mockclock.MockClock.at
import io.kotest.core.listeners.TestListener
import io.kotest.core.spec.style.StringSpec
import io.kotest.core.test.TestCase
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Database.Companion.connect
import java.time.Instant.parse
import java.time.ZoneId.of

abstract class IntegrationTest(body: IntegrationTest.() -> Unit = {}) : StringSpec() {

  val clock: MockClock = at(parse("2020-01-30T09:59:59.123Z"), of("America/Montreal"))
  lateinit var database: Database

  init {
    listener(object : TestListener {
      override suspend fun beforeTest(testCase: TestCase) {
        val name = testCase.description.fullName().replace(Regex("[^A-Za-z0-9]"), "")
        val databaseUrl = "jdbc:h2:mem:$name;DB_CLOSE_DELAY=-1"
        database = connect(databaseUrl)
      }
    })
    body()
  }
}
