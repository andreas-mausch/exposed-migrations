# About

This library is a super-basic migration tool for the Kotlin SQL Framework Exposed by Jetbrains.

See this issue: https://github.com/JetBrains/Exposed/issues/165

Currently, only migrations to a higher version are possible, downgrades are not.

# Sample usage

```kotlin
class V1_SampleMigration : Migration() {
  object SomeTestTable : IntIdTable() {
    val someField = integer("someField")

    init {
      index(true, someField)
    }
  }

  override fun run() {
    SchemaUtils.create(SomeTestTable)
  }
}
```

and

```kotlin
runMigrations(database, listOf(V1_SampleMigration()), Clock.systemUTC())
```

Maven dependency:

```xml
<dependency>
  <groupId>de.neonew</groupId>
  <artifactId>exposed-migrations</artifactId>
  <version>${versions.exposed-migrations}</version>
</dependency>
```

# SQL details

A table named `MIGRATIONS` is used to store all executed migrations.
It is used to find the current state of the database and to determine which migrations still need to be executed.
