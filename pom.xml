<?xml version="1.0" encoding="UTF-8"?>
<project
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
  xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <modelVersion>4.0.0</modelVersion>

  <groupId>de.neonew</groupId>
  <artifactId>exposed-migrations</artifactId>
  <version>2.0.0</version>

  <name>${project.groupId}:${project.artifactId}</name>
  <description>A super-basic migration tool for the Kotlin SQL Framework Exposed by JetBrains</description>
  <url>https://gitlab.com/andreas-mausch/exposed-migrations</url>

  <licenses>
    <license>
      <name>MIT License</name>
      <url>https://gitlab.com/andreas-mausch/exposed-migrations/-/raw/master/LICENSE</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <name>Andreas Mausch</name>
      <email>andreas.mausch@gmail.com</email>
      <organization>GitLab</organization>
      <organizationUrl>https://gitlab.com/andreas-mausch</organizationUrl>
    </developer>
  </developers>

  <scm>
    <connection>scm:git:https://gitlab.com/andreas-mausch/exposed-migrations.git</connection>
    <developerConnection>scm:git:git@gitlab.com:andreas-mausch/exposed-migrations.git</developerConnection>
    <url>https://gitlab.com/andreas-mausch/exposed-migrations</url>
    <tag>HEAD</tag>
  </scm>

  <properties>
    <kotlin.compiler.languageVersion>1.3</kotlin.compiler.languageVersion>
    <kotlin.compiler.incremental>true</kotlin.compiler.incremental>
    <kotlin.compiler.jvmTarget>13</kotlin.compiler.jvmTarget>

    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <versions.kotlin>1.3.72</versions.kotlin>

    <versions.exposed>0.24.1</versions.exposed>
    <versions.h2>1.4.200</versions.h2>
    <versions.kotest>4.0.5</versions.kotest>
    <versions.kotlin-logging>1.7.9</versions.kotlin-logging>
    <versions.mock-clock>1.0</versions.mock-clock>
    <versions.slf4j>1.7.30</versions.slf4j>

    <versions.plugins.build-helper>3.1.0</versions.plugins.build-helper>
    <versions.plugins.git-commit-id>4.0.0</versions.plugins.git-commit-id>
    <versions.plugins.maven-enforcer>3.0.0-M3</versions.plugins.maven-enforcer>
    <versions.plugins.maven-failsafe>3.0.0-M4</versions.plugins.maven-failsafe>
    <versions.plugins.maven-gpg>1.6</versions.plugins.maven-gpg>
    <versions.plugins.maven-jar>3.2.0</versions.plugins.maven-jar>
    <versions.plugins.maven-source>3.2.1</versions.plugins.maven-source>
    <versions.plugins.maven-surefire>3.0.0-M4</versions.plugins.maven-surefire>
    <versions.plugins.maven-versions>2.7</versions.plugins.maven-versions>
    <versions.plugins.nexus-staging>1.6.8</versions.plugins.nexus-staging>
  </properties>

  <repositories>
    <repository>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>central</id>
      <name>bintray</name>
      <url>https://jcenter.bintray.com</url>
    </repository>
  </repositories>

  <dependencies>
    <!-- Production -->

    <dependency>
      <groupId>org.jetbrains.kotlin</groupId>
      <artifactId>kotlin-stdlib</artifactId>
      <version>${versions.kotlin}</version>
    </dependency>

    <dependency>
      <groupId>io.github.microutils</groupId>
      <artifactId>kotlin-logging</artifactId>
      <version>${versions.kotlin-logging}</version>
    </dependency>

    <dependency>
      <groupId>org.jetbrains.exposed</groupId>
      <artifactId>exposed-core</artifactId>
      <version>${versions.exposed}</version>
    </dependency>
    <dependency>
      <groupId>org.jetbrains.exposed</groupId>
      <artifactId>exposed-dao</artifactId>
      <version>${versions.exposed}</version>
    </dependency>
    <dependency>
      <groupId>org.jetbrains.exposed</groupId>
      <artifactId>exposed-jdbc</artifactId>
      <version>${versions.exposed}</version>
    </dependency>
    <dependency>
      <groupId>org.jetbrains.exposed</groupId>
      <artifactId>exposed-java-time</artifactId>
      <version>${versions.exposed}</version>
    </dependency>

    <!-- Test -->

    <dependency>
      <groupId>io.kotest</groupId>
      <artifactId>kotest-runner-junit5-jvm</artifactId>
      <version>${versions.kotest}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>io.kotest</groupId>
      <artifactId>kotest-assertions-core-jvm</artifactId>
      <version>${versions.kotest}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>io.kotest</groupId>
      <artifactId>kotest-property-jvm</artifactId>
      <version>${versions.kotest}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <version>${versions.h2}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-simple</artifactId>
      <version>1.7.30</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>com.statemachinesystems</groupId>
      <artifactId>mock-clock</artifactId>
      <version>${versions.mock-clock}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <sourceDirectory>${project.basedir}/source</sourceDirectory>
    <testSourceDirectory>${project.basedir}/test/unit</testSourceDirectory>
    <resources>
      <resource>
        <directory>
          ${project.basedir}/resources
        </directory>
      </resource>
    </resources>

    <plugins>
      <plugin>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-maven-plugin</artifactId>
        <version>${versions.kotlin}</version>

        <executions>
          <execution>
            <id>compile</id>
            <goals>
              <goal>compile</goal>
            </goals>
          </execution>

          <execution>
            <id>test-compile</id>
            <goals>
              <goal>test-compile</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>pl.project13.maven</groupId>
        <artifactId>git-commit-id-plugin</artifactId>
        <version>${versions.plugins.git-commit-id}</version>
        <executions>
          <execution>
            <id>get-the-git-infos</id>
            <goals>
              <goal>revision</goal>
            </goals>
            <phase>initialize</phase>
          </execution>
        </executions>
        <configuration>
          <generateGitPropertiesFile>true</generateGitPropertiesFile>
          <gitDescribe>
            <tags>true</tags>
          </gitDescribe>
          <includeOnlyProperties>
            <includeOnlyProperty>git.commit.id.abbrev</includeOnlyProperty>
            <includeOnlyProperty>git.commit.id.describe</includeOnlyProperty>
            <includeOnlyProperty>git.commit.id.describe-short</includeOnlyProperty>
            <includeOnlyProperty>git.commit.time</includeOnlyProperty>
            <includeOnlyProperty>git.tags</includeOnlyProperty>
          </includeOnlyProperties>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <version>${versions.plugins.maven-enforcer}</version>
        <executions>
          <execution>
            <id>enforce-maven</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <requireMavenVersion>
                  <version>3.6</version>
                </requireMavenVersion>
              </rules>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>build-helper-maven-plugin</artifactId>
        <version>${versions.plugins.build-helper}</version>
        <executions>
          <execution>
            <id>add-integration-test-source-as-test-sources</id>
            <phase>generate-test-sources</phase>
            <goals>
              <goal>add-test-source</goal>
            </goals>
            <configuration>
              <sources>
                <source>test/integration</source>
              </sources>
            </configuration>
          </execution>
          <execution>
            <id>add-test-resource</id>
            <phase>generate-test-resources</phase>
            <goals>
              <goal>add-test-resource</goal>
            </goals>
            <configuration>
              <resources>
                <resource>
                  <directory>test/resources</directory>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${versions.plugins.maven-surefire}</version>
        <configuration>
          <includes>
            <include>**/tests/unit/**/*.java</include>
          </includes>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>${versions.plugins.maven-failsafe}</version>
        <executions>
          <execution>
            <goals>
              <goal>integration-test</goal>
              <goal>verify</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <includes>
            <include>**/tests/integration/**/*.java</include>
          </includes>
          <trimStackTrace>false</trimStackTrace>
          <systemPropertyVariables>
            <org.slf4j.simpleLogger.defaultLogLevel>debug</org.slf4j.simpleLogger.defaultLogLevel>
          </systemPropertyVariables>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>versions-maven-plugin</artifactId>
        <version>${versions.plugins.maven-versions}</version>
        <configuration>
          <rulesUri>file:///${project.basedir}/version-rules.xml</rulesUri>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <id>release</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>${versions.plugins.maven-jar}</version>
            <executions>
              <execution>
                <id>javadoc-jar</id>
                <phase>package</phase>
                <goals>
                  <goal>jar</goal>
                </goals>
                <configuration>
                  <classifier>javadoc</classifier>
                  <classesDirectory>${project.basedir}/javadoc</classesDirectory>
                </configuration>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <version>${versions.plugins.maven-source}</version>
            <executions>
              <execution>
                <id>attach-sources</id>
                <goals>
                  <goal>jar</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-gpg-plugin</artifactId>
            <version>${versions.plugins.maven-gpg}</version>
            <executions>
              <execution>
                <id>sign-artifacts</id>
                <phase>verify</phase>
                <goals>
                  <goal>sign</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.sonatype.plugins</groupId>
            <artifactId>nexus-staging-maven-plugin</artifactId>
            <version>${versions.plugins.nexus-staging}</version>
            <extensions>true</extensions>
            <configuration>
              <serverId>ossrh</serverId>
              <nexusUrl>https://oss.sonatype.org/</nexusUrl>
              <autoReleaseAfterClose>true</autoReleaseAfterClose>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
