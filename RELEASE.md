# Release

## Set up GPG key

https://keys.openpgp.org/search?q=1089A1DEC44DA1AF5185340BBB7DA3520E72978E

Install private key on your system.

`gpg --list-keys` should return the id above.

## Maven settings

*~/.m2/settings.xml*

```xml
<settings xmlns="http://maven.apache.org/settings/1.0.0">
    <servers>
        <server>
            <id>ossrh</id>
            <username>andreas-mausch</username>
            <password>${env.SONATYPE_PASSWORD}</password>
        </server>
    </servers>
    <profiles>
        <profile>
            <id>ossrh</id>
            <properties>
                <gpg.keyname>1089A1DEC44DA1AF5185340BBB7DA3520E72978E</gpg.keyname>
                <gpg.passphraseServerId>${gpg.keyname}</gpg.passphraseServerId>
                <gpg.defaultKeyring>false</gpg.defaultKeyring>
            </properties>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>ossrh</activeProfile>
    </activeProfiles>
</settings>
```

## Update version

```
mvn versions:set -DnewVersion=x.x.x -DgenerateBackupPoms=false
```

## Check everything is working

```
mvn clean install -Prelease
```

## Deploy to Maven Central

```
env SONATYPE_PASSWORD=secret mvn clean deploy -Prelease
```

## Tag

```
git tag vx.x.x
git push origin vx.x.x
```
