package de.neonew.exposed.migrations

import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Clock
import java.time.Instant.now

private val logger = KotlinLogging.logger {}

fun runMigrations(database: Database, migrations: List<Migration>, clock: Clock) {
  checkVersions(migrations)

  logger.info { "Running migrations on database ${database.url}" }

  val latestVersion = transaction(database) {
    createTableIfNotExists(database)
    MigrationEntity.all().maxBy { it.version }?.version?.value
  }

  logger.info { "Database version before migrations: $latestVersion" }

  migrations
    .sortedBy { it.version }
    .filter { shouldRun(latestVersion, it) }
    .forEach {
      logger.info { "Running migration version ${it.version}: ${it.name}" }
      transaction(database) {
        it.run()

        MigrationEntity.new {
          version = EntityID(it.version, Migrations)
          name = it.name
          executedAt = now(clock)
        }
      }
    }

  logger.info { "Migrations finished successfully" }
}

private fun checkVersions(migrations: List<Migration>) {
  val sorted = migrations.map { it.version }.sorted()
  if ((1..migrations.size).toList() != sorted) {
    throw IllegalStateException("List of migrations version is not consecutive: $sorted")
  }
}

private fun createTableIfNotExists(database: Database) {
  if (Migrations.exists()) {
    return
  }
  val tableNames = database.dialect.allTablesNames()
  when (tableNames.isEmpty()) {
    true -> {
      logger.info { "Empty database found, creating table for migrations" }
      create(Migrations)
    }
    false -> throw IllegalStateException("Tried to run migrations against a non-empty database without a Migrations table. This is not supported.")
  }
}

private fun shouldRun(latestVersion: Int?, migration: Migration): Boolean {
  val run = latestVersion?.let { migration.version > it } ?: true
  if (!run) {
    logger.debug { "Skipping migration version ${migration.version}: ${migration.name}" }
  }
  return run
}
