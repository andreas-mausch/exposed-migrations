package de.neonew.exposed.migrations

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.`java-time`.timestamp

object Migrations : IdTable<Int>() {
  override val id = integer("version").entityId()
  override val primaryKey = PrimaryKey(id)

  val name = varchar("name", length = 400)
  val executedAt = timestamp("executed_at")

  init {
    index(true, name)
  }
}

class MigrationEntity(id: EntityID<Int>) : IntEntity(id) {
  companion object : IntEntityClass<MigrationEntity>(Migrations)

  var version by Migrations.id
  var name by Migrations.name
  var executedAt by Migrations.executedAt
}
